package klem.wedoogift.mastermind;

public class App {

    public static final int  SERVING_SIZE = 4;
    public static final int  TRIES = 10;

    public static void main(String[] args) {
        new Game(TRIES, SERVING_SIZE).start();
    }
}
