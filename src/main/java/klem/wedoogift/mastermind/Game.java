package klem.wedoogift.mastermind;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.regex.Pattern;

public class Game {

    private static int tries;
    private static int servingSize;
    private int currentTurn = 1;
    private IOHandler io;
    private Pattern pattern = null;
    private Resolver resolver;
    private List<Guess> guesses = new ArrayList<>();

    public Game(int tries, int servingSize) {
        this.io = new IOHandler();
        Game.tries = tries;
        Game.servingSize = servingSize;
        this.resolver = new Resolver(servingSize);
        this.pattern = Pattern.compile("[RJBOVN]{"+servingSize+"}+");
    }


    public void start() {



        System.err.println(resolver.getAnswer());

        io.write(
                "Welcome to master mind!\n" +
                "You must resolve the combination I just made up.\n" +
                "Enter any first letter, %d chars long combination you like from the following list\n" +
                " %s\n" +
                "You have %d tries.\n" +
                "Enter your guess >", servingSize, EnumSet.allOf(Token.class), tries);

        printHeader();

        Guess guess = new Guess(io.read(), 1);
        boolean success = validateGuess(guess);

        System.exit(success ? 0 : -1);

    }

    private void printHeader() {
        io.write("|--I---M---C----T---|");
        io.write(String.format("|....| . | . | %d/%d |", currentTurn , tries));
        io.write("|-------------------|");
    }

    private boolean validateGuess(Guess guess) {
        boolean success;

        if (!pattern.matcher(guess.getInput()).matches()){
            io.write("Invalid input! Can only contain any of  RJBOVN must be %d chars long!\n" +
                    " Your turn was not wasted.\n" +
                    " Enter your guess >", servingSize);
            validateGuess(new Guess(io.read(), currentTurn)); // NO TURN INCREMENT FOR INPUT ERRORS
        }

        guesses.add(guess);
        success = resolver.evaluate(guess);
        printGuesses();

        if (guess.getTurnCount() == tries && !success) {
            io.write("The Game : you just lost\n" +
                    " Answer was : %s", resolver.toString());
            return false;
        }

        if (success) {
            io.write("Congrats! You won in %s turns", currentTurn);

        } else {
            io.write("Failed! Try again >");
            success = validateGuess(new Guess(io.read(), ++currentTurn));
        }


        return success;
    }

    private void printGuesses() {
        io.write("|--I---M---C----T---|");
        guesses.forEach(g-> io.write(g.toString()));

        if(currentTurn != tries) {
            io.write(String.format("|....| . | . | %d/%d |", currentTurn +1, tries));
        }

        io.write("|-------------------|");
    }
}
